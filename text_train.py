from textgenrnn import textgenrnn

textgen = textgenrnn()
textgen.train_from_file('results_title.csv', num_epochs=60)

textgen.generate()

# either dump model to gcp bucket and automatically download in serverless application or 
# keep in other repo.
textgen.save('model.hd5')

